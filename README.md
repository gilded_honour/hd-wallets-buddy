# HD wallets buddy

This buddy generates HD wallets off a mnemonic phrase.

#### How to

Create a configuration file `.env` and fill it out.

```
cp .env.example .env
```

Then run it:

```
node wallet_generator.js --start-index 35 --count 10 --blockchain tron
```

#### Note

In order to be able to re-generate your keys, save somewhere not only your mnemonic phrase but the derivation path too. The latter is subject to change.
import dotenv from 'dotenv';
import minimist from 'minimist';
import ethers from 'ethers';
import tronWeb from 'tronweb';

export const TRON_BLOCKCHAIN = "tron";
export const ETHEREUM_BLOCKCHAIN = "ethereum";
export const SOLANA_BLOCKCHAIN = "solana";
export const DEFAULT_BLOCKCHAIN = ETHEREUM_BLOCKCHAIN;
export const DERIVATION_PATH = {
  ethereum: `m/44'/60'/0'/0`,
  solana:   `m/44'/501'/0'/0'`,
  tron:     `m/44'/195'/0'/0'`
  // tron:     `m/44'/195'/0'/0`
};


dotenv.config();

let args = minimist(process.argv.slice(2));
const node = ethers.utils.HDNode.fromMnemonic(process.env.MNEMONIC);
const startIndex = args["start-index"] || 0;
const count = args["count"] || 30;
let nw = args["blockchain"];
const SUPPORTED_BLOCKCHAINS = [
  TRON_BLOCKCHAIN,
  ETHEREUM_BLOCKCHAIN,
  //SOLANA_BLOCKCHAIN,
];

if (nw === undefined) {
  nw = DEFAULT_BLOCKCHAIN;
} else {
  if (!SUPPORTED_BLOCKCHAINS.includes(nw)) {
    throw new Error(`unsupported blockchain: ${nw}`);
  }
}

let dp = DERIVATION_PATH[nw];
let endIndex = startIndex + count;
const wallets = [];
for (let i = startIndex; i < endIndex; i++) {
  const wallet = node.derivePath(`${dp}/${i}`);
  wallets.push(wallet);
}

//print them
console.log(`\n----------------------------------`)

console.log(`blockchain: ${nw.toUpperCase()}`)
console.log(`derivation path: ${dp}`)
console.log(`counter: ${startIndex}..${endIndex} (${wallets.length} wallets)`)

console.log(`----------------------------------\n`)


for (const wallet of wallets) {
  if (nw === TRON_BLOCKCHAIN) {
    let b58Addr = tronWeb.address.fromHex(wallet.address);
    console.log(`address:\t${b58Addr}`);
  } else {
    console.log(`address:\t${wallet.address}`);
  }

  console.log(`privateKey:\t${wallet.privateKey}`);
  console.log('\n');
}


console.log(`\n-----------------public keys only (${wallets.length} wallets)-----------------\n`)
for (const wallet of wallets) {
  if (nw === TRON_BLOCKCHAIN) {
    let b58Addr = tronWeb.address.fromHex(wallet.address);
    console.log(b58Addr);
  } else {
    console.log(wallet.address);
  }
}
console.log('\n');
